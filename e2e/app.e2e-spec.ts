import { JinpaiPage } from './app.po';

describe('jinpai App', () => {
  let page: JinpaiPage;

  beforeEach(() => {
    page = new JinpaiPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
