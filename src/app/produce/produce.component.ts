import { Component, OnInit } from '@angular/core';
import {Produce, ProduceService} from "../share/produce.service";
import 'rxjs/Rx';
import {FormControl} from "@angular/forms";
import {Observable} from "rxjs/Observable";

@Component({
  selector: 'app-produce',
  templateUrl: './produce.component.html',
  styleUrls: ['./produce.component.css']
})
export class ProduceComponent implements OnInit {
  public keyword :string;
  //通过管道使搜索相对应的商品
  private titleFilter:FormControl = new FormControl();
  public produces :Observable<Produce[]>;
  public imgUrl:string="http://placehold.it/320x150";
  constructor(
    private produceService:ProduceService

  ) {
    //在使用dubounceTime的时候需要引入 rxjs/Rx 这里的意思是订阅页面上的titleFilter变化的流,延迟500毫秒响应搜索,页面上需要属性绑定
    //在使用响应式的表单的时候需要在app.module中引入ReactiveFormsModule
    this.titleFilter.valueChanges.debounceTime(500).subscribe((value)=>{
      this.keyword=value;
    })
  }

  ngOnInit() {
    this.produces=this.produceService.getProduces()

    this.produceService.searchEvent.subscribe(
      params => this.produces = this.produceService.search(params)
    )

  }

}




