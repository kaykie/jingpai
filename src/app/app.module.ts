import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { SearchComponent } from './search/search.component';
import { CarouselComponent } from './carousel/carousel.component';
import { FooterComponent } from './footer/footer.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ProduceComponent } from './produce/produce.component';
import { StarsComponent } from './stars/stars.component';
import { ProduceDetailComponent } from './produce-detail/produce-detail.component';
import { HomeComponent } from './home/home.component';
import {RouterModule, Routes} from "@angular/router";
import {ProduceService} from "./share/produce.service";
import { FilterPipe } from './pipe/filter.pipe';
//配置路由
const routeConfig:Routes =[
  {path:'produce/:prodId',component:ProduceDetailComponent},
  {path:'Home',component:HomeComponent},
  {path:'',redirectTo:'Home',pathMatch:'full'}

]


@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    CarouselComponent,
    FooterComponent,
    NavbarComponent,
    ProduceComponent,
    StarsComponent,
    ProduceDetailComponent,
    HomeComponent,
    FilterPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routeConfig)
  ],
  //要使用依赖注入需要在providers中引入服务才能使用
  providers: [ProduceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
