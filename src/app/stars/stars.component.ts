import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-stars',
  templateUrl: './stars.component.html',
  styleUrls: ['./stars.component.css']
})
export class StarsComponent implements OnInit,OnChanges{
  ngOnChanges(changes: SimpleChanges): void {
    this.stars=[];
    for(let i=1;i<=5;i++){
      this.stars.push(i>this.rating)
    }
  }

  //因为rating是输入属性,当这个变化的时候会触发Onchange的生命钩子
  @Input()
  private rating:number=0;
  //将this.rating发射出去
  @Output()
  private ratingChange:EventEmitter<number>=new EventEmitter();

  private stars:boolean[];

  @Input()
  public readonly :boolean=true;

  constructor() { }

  ngOnInit() {

  }

  clickStar(index:number){
    //如果可以点击才发射出this.rating,只有当ratingChange 和需要发射的值rating相同时才能使用双向绑定方式.
    if(!this.readonly){
      this.rating =index +1;
      this.ratingChange.emit(this.rating)
    }
  }
}
