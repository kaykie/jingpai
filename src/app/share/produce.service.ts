import {EventEmitter, Injectable} from '@angular/core';
import {Http, URLSearchParams} from "@angular/http";
import "rxjs/Rx";
import {Observable} from "rxjs/Observable";
@Injectable()
export class ProduceService {
  searchEvent:EventEmitter<ProduceSearchParams>=new EventEmitter()

//   private produces :Produce[]= [
//   new Produce(1,"第一个商品",1.99,1.5,"1商品的描述商品的描述商品的描述商品的描述商品的描述商品的描述",["电子",'硬件']),
//   new Produce(2,"第二个商品",2.99,4.5,"2商品的描述商品的描述商品的描述商品的描述商品的描述商品的描述",["电子",'图书']),
//   new Produce(3,"第三个商品",3.99,2.5,"3商品的描述商品的描述商品的描述商品的描述商品的描述商品的描述",["电子",'硬件']),
//   new Produce(4,"第四个商品",4.99,3.5,"4商品的描述商品的描述商品的描述商品的描述商品的描述商品的描述",['硬件']),
//   new Produce(5,"第五个商品",5.99,4.5,"5商品的描述商品的描述商品的描述商品的描述商品的描述商品的描述",["电子"]),
//   new Produce(6,"第六个商品",6.99,2.5,"6商品的描述商品的描述商品的描述商品的描述商品的描述商品的描述",["电子"]),
// ]
  constructor(
    private http:Http
  ) { }

  getProduces():Observable<Produce[]>{
    //注意箭头函数不要加花括号
    return this.http.get('/api/products').map(res=>res.json())
    // return this.produces
  }


  getAllCategories():string[]{
    return ['电子','硬件','图书'];
  }



  getProduce( id:number){
    // return this.produces.find((produce)=>produce.id == id)
    return this.http.get('/api/products/'+id).map(res=>res.json())
  }


  // private comments:Comment[]=[
  //   new Comment(1,1,'2017-2-2 2:2:2','张三',3,"东西不错"),
  //   new Comment(2,1,'2017-2-3 22:12:22','张三',3,"东西不错"),
  //   new Comment(3,1,'2017-2-4 12:12:12','张三',2,"东西不错"),
  //   new Comment(4,2,'2017-2-5 2:2:2','张三',4,"东西不错"),
  //
  // ]
  getCommentsForProduceId(id:number){
    console.log('/api/products/'+id+"/comments")
    return this.http.get('/api/products/'+id+"/comments").map(res=>res.json())
  }

  search(params:ProduceSearchParams):Observable<Produce[]>{
    return this.http.get("/api/products",{search:this.encodeParams(params)}).map(res=> res.json())
  }

  private encodeParams(params: ProduceSearchParams) {
    // console.log(1,params,1.5,Object.keys(params),2,Object.keys(params).filter(key => params[key]),3,Object.keys(params).filter(key => params[key]).reduce((sum:URLSearchParams,key:string) =>{
    //   sum.append(key,params[key]);
    //   return sum;
    // },new URLSearchParams));
    // let result:URLSearchParams;
    //第一个表示会把所有的参数的key值拿出来变成一个数组集合
    //第二个表示过滤传入参数中没有值的对象,filter具体的方式,参考百度
    //第三个 首先new URLSearchParams并把sum类型定义为此,sum中有一个方法,append
    // result =
    return Object.keys(params)
      .filter(key => params[key])
      .reduce((sum:URLSearchParams,key:string) =>{
        sum.append(key,params[key]);
        return sum;
      },new URLSearchParams);
  }


}



export class Produce{
  constructor(
    public id:number,
    public title:string,
    public price:number,
    public rating:number,
    public desc:string,
    public categories:Array<string>
  ){}
}

export class Comment{
  constructor(
    private id:number,
    public produceId:number,
    public timestamp:string,
    public user:string,
    public rating:number,
    public content:string
  ){}
}







export class ProduceSearchParams{
  constructor(
    public title:string,
    public price:number,
    public category:string
  ){}
}







