import { TestBed, inject } from '@angular/core/testing';

import { ProduceService } from './produce.service';

describe('ProduceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProduceService]
    });
  });

  it('should be created', inject([ProduceService], (service: ProduceService) => {
    expect(service).toBeTruthy();
  }));
});
